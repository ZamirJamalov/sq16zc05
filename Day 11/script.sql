

--Bugune kimi ancaq  bir select den ibaret ifadeler yazmisdiq ( set operatorlar xaric)

--bezen ise bir nece select ifadelerin bir yerde yazilmasi lazim olur.

--bunun ucun subquery lerden istifade olunur.

--subquery dedikde bir sorgunun daxilinde basqa bir sorgu ve ya sorgularin yazilmasi basa dusulur.

--diger sorgunun daxilinde olan sorguya termin olaraq  inner/ sub query  daha ust sorguya ise main/outer query  adlandirilirlar.

--subquery ile yazilan sorgular ekser hallarda bildiyiiz  join lerle de evez etmek mumkundur.


--subquery bir nece nove bolunurler. misal olaraq,  scalar ve nornscalar subqueryler 

--scalar subquery- icra olunduqda YALNIZ 1 setr 1 sutundan ibaret deyer qaytarir. misal olaraq:

--select department_id from departments where department_name='IT'

--bu ise nonscalar sorgudur,  baxmayarq ki, 1 setr  netice qaytarir

--select department_id, department_name from departments where department_name='IT'

--scalar query ni subqeury olaraq istifade edek:
--misal olaraq, IT departmentde calisan emekdaslarin siyahisini alaq:


 SELECT  first_name, last_name FROM  employees 
   WHERE  department_id=(SELECT department_id  FROM departments 
                               WHERE department_name='IT')





--bu sorgunun isleme ardicilligina nezer salaq:

--ilk olaraq, deye bilerik ki, inner subquery avtonom olaraq, calisa bilir, 
--Yani inner query ayrica icra etsek bize netice qqaytaracaq.  Bu  xususiyyet yalniz SCALAR  ve NONSCALAR 
--aiddir. CORRELATED sorgulara ise bu aid deyil. Bu nov subquerylere az sonra  baxacagiq.

--isleme ardicilligi bu sekildedir:

--1. ilk olaraq subquery icra olunur  SELECT department_id  FROM departments WHERE department_name='IT'
--2. 1-de alinan netice subquery - ile evez  edilir.
   -- SELECT  first_name, last_name FROM  employees  WHERE  department_id=30 (dogru deyer hansidirsa )
--3. 2-de olan sql icra olunur.

--buradan bunu anlamaq olar ki, inner sorgu , main sorgunun her setri ucun ayri-ayri icra olunmur. 
--yalniz bir defe icra edilir, onun deyeri nedirse  , hemin deyer daha sonra istifade olunur.

--O zaman deye bilerik ki, subquery isini bildiyimiz qaydalara ugun olaraq ,  it department ID sini elde edib,

--SELECT first_name,last_name FROM employees WHERE department_id=30 da yazaraq hell etmek mumkundur.

--Beli, bu sonda istediyimiz netice qaytaracaq. Ancaq bu yanasma her zaman qebul  olunmur.  Misal olaraq:

--Butun emkdaslarin ortalama emek haqqisindan daha cox  emek haqqi ala emekdaslarin siyahisini elde etmek lazimdir.

--Buna bildiyimiz qayda ile yanassaq , ilk novbede ortalama emek haqqi nedir onu tapmagimiz lazimdir.

--SELECT AVG(salary) FROM employees;

--Sorgunun bu sekilde sade olmasinin sebebi bize verilen tapsiriqda butun emekdaslara gore deyildiyiucundur. 
--Burada istifade etdiyimiz AVG ededi ortanin tapilmasi ucun istifade etdiyimiz  aggregate funksiyadir.

--ededi ortani tapdiqdan sonra, tapsiriga uygun  olaraq yaza bilerik:

--SELECT first_name,last_name FROM employees WHERE salary>6461.636

--bu sorgu verilen tapsirigiicra etmek ucun kifayetdir. 
--Ancaq bu tapsiriq bize, 1 hefte sonra  yene daxil olsa, o zmaan salary>6461.636 bezi sebeblerden aktual olmayacaq.

--bu seedden biz yeninden ededi ordani tapmaq ucun bir evvelki sorgunu icra etmeli ve aldigimiz yeni deyeri 
--sonuncu sorguda tetbiq  edib icra etmeliyik. Bir sozle , eyni bir isi her zaman manual sekilde gormeye mecburuq ve burada 
--isin avtomatlasdirilmasinda  soz gede bilmez,

--burada cixis yolu bu iki sql bir-birinin daxilnde  yazmaqdir. 


  SELECT first_name, last_name FROM  employees 
    WHERE salary>(SELECT  AVG(salary) FROM employees);



--Bu sorguda yuxarida qeyd olunmus sorgudaki kimi isleme ardicilligina malikdir. Evvelki manual yanasmadan ferqli olaraq,
--burada isi tam avtomatlasdirmaq mumkundur artiq. Yani bu sorgunun ne zaman icra etsek ,
--hemin zaman uygun melumati bize qaytaracaq.

--bu misalin timsalinda biz  =,<=,<,>=,> kimi sertleri tetbiq ede bilerik


--Eger edei ortani butun emekdaslara gore deyil, her department e gore etsek , o zaman 
--SCALAR deyil NON SCALAR sorgudan istifaede edeceyik:
--Misal olaraq, her departamentde olan en yuksek emek haqqini departmentde calisanlardan hansilari bu emek haqqini
--alirsa onlarin siyahisi lazimdir.




  SELECT first_name, last_name 
    FROM employees  
      WHERE  (department_id, salary) IN 
        (SELECT department_id, max(salary) FROM employees GROUP BY department_id);



--burada da subqery avtonom calisdirsaq bize netice qaytaracaqdir. ve  qaytatilen bu coxluqdaki her setr main query 
--de verilen serte uygunlasdirilir ve eger uygundursa o zaman uygun melumatlar netice qaytarilir.



--Basqa bir misalda: Emekdas calismayan departmentlerin siyahisini elde etmek lazimdir:
--basqa sekilde desek, departments cedvelinde  olub, emmployees cedvelinde olmayan setrleri tapmaq lazimdir




 SELECT * FROM departnets
   WHERE department_id NOT IN 
     (SELECT department_id FROM employees);



--Bu srogunu icra etsek. bize netice qaytarmayacaqdir. 
--Buba sebeb iiner query nin qaytardigi setrlerde department_id sutunda NULL olan setrin olmadsidir.

--Bunu hell etmek ucun innser query de  department_id IS NOT NULL serti elave etmek lazimdir. Netice olaraq,



  SELECT * FROM departments 
    WHERE department_id NOT IN 
     (SELECT department_id FROM employees WHERE department_id IS NOT NULL);




--Bu sorgunu JOIN ile eveze ede bilerik:



 SELECT * FROM departments 
  LEFT JOIN employees ON departments.department_id=employees.employee_id  
     WHERE employees.department_id is null;




--CORRELATED qubquery

--misal olaraq:  her department uzre ortalama emk haqqidan daha emek haqqi alan emekdaslarin siyahisini almaq lazim.

--burada inner query artiq avtonom deyil, main query nin her bir setrinden asili olaraq isleyecekdir.



 SELECT * FROM employees  e
   WHERE salary>(SELECT AVG(salary) FROM employees e2 
                       WHERE e2.department_id=e.department_id);




     

--Sub-queries in the Where-statement  [NON-SCALAR SUBQUERY]
--ORA-00913: too many values
 
 SELECT E.FIRST_NAME,
        E.LAST_NAME 
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID = (SELECT D.DEPARTMENT_ID,
  								  D.DEPARTMENT_NAME 
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME ='IT'
  					      );

  					     
 --Sub-queries in the Where-statement  [NON-SCALAR SUBQUERY]
 --ORA-01427: single-row subquery returns more than one row
 
 SELECT E.FIRST_NAME,
        E.LAST_NAME 
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID = (SELECT D.DEPARTMENT_ID
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME ='IT'
  							   OR D.DEPARTMENT_NAME ='Administration'
  					      );  
  					     
--SOLUTION   ANY|ALL
  					     
 SELECT E.FIRST_NAME,
        E.LAST_NAME ,
        e.department_id
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID = ANY(SELECT D.DEPARTMENT_ID
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME ='IT'
  							   OR D.DEPARTMENT_NAME ='Administration'
  					      );  					
 
SELECT E.FIRST_NAME,
        E.LAST_NAME ,
        e.department_id
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID <> ALL(SELECT D.DEPARTMENT_ID
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME ='IT'
  						       OR D.DEPARTMENT_NAME ='Administration'
  					      ); 
--SOLUTION IN|NOT IN 

 SELECT E.FIRST_NAME,
        E.LAST_NAME ,
        e.department_id
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID NOT IN (SELECT D.DEPARTMENT_ID
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME IN ('IT','Administration')
  					      );    
 SELECT E.FIRST_NAME,
        E.LAST_NAME ,
         e.department_id
   FROM EMPLOYEES E 
  WHERE E.DEPARTMENT_ID NOT IN (SELECT D.DEPARTMENT_ID
  							 FROM DEPARTMENTS D
  							WHERE D.DEPARTMENT_NAME ='IT'
  							   OR D.DEPARTMENT_NAME ='Administration'
  					      );  
SELECT * FROM EMPLOYEES e ;
  			
--Sub-queries in the Join-statement
  					     
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       J.CITY
  FROM EMPLOYEES E 
  JOIN (SELECT D.DEPARTMENT_ID,
               L.CITY
          FROM DEPARTMENTS D
          JOIN LOCATIONS L
            ON D.LOCATION_ID = L.LOCATION_ID) J
    ON E.DEPARTMENT_ID = J.DEPARTMENT_ID;  
    
   
SELECT e.first_name,
       e.last_name,
       l.city 
  FROM EMPLOYEES e 
  JOIN DEPARTMENTS d 
    ON e.DEPARTMENT_ID = d.DEPARTMENT_ID 
  JOIN LOCATIONS l 
    ON d.LOCATION_ID = l.LOCATION_ID 
    
--Nested sub-queries
   
SELECT
	E.FIRST_NAME,
	E.LAST_NAME ,
	E.department_id
FROM
	(
	SELECT
		FIRST_NAME,
		LAST_NAME,
		department_id
	FROM
		EMPLOYEES
	GROUP BY
		FIRST_NAME,
		LAST_NAME,
		department_id
	HAVING
		LENGTH(FIRST_NAME) > 4
		AND LENGTH(LAST_NAME) > 5) E
WHERE
	e.department_id = 100;
       
       

 --CORRELATED SUBQUERY
--Əmək haqqısı bütün əməkdaşlar üzrə orta əmək haqqıdan çox olan əməkdaşların siyahısı       
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       E.SALARY
  FROM EMPLOYEES E
 WHERE E.SALARY >(SELECT AVG(e2.SALARY) FROM EMPLOYEES e2)            ;

--Əmək haqqısı çalışdığı departament üzrə orta əmək haqqıdan çox olan əməkdaşların siyahısı              
                    
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       E.SALARY,
       e.department_id
  FROM EMPLOYEES E
 WHERE E.SALARY > (SELECT AVG(SALARY)
                     FROM EMPLOYEES EE
                    WHERE EE.DEPARTMENT_ID =e.department_id);
       
 SELECT * FROM EMPLOYEES e  WHERE e.DEPARTMENT_ID =60;
                   
                   
--EXİSTS/ NOT EXİSTS
--Vəzifəsini dəyişmiş əməkdaşların siyahısı
 
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       E.JOB_ID,
       E.EMPLOYEE_ID 
  FROM EMPLOYEES E 
 WHERE exists (SELECT 1
                 FROM JOB_HISTORY JH
                WHERE JH.EMPLOYEE_ID = E.EMPLOYEE_ID);
            
               
--Qeyd: Yuxarıda JOB_HISTORY  cədvəlində təkrar employee_id olduğu halda bunlar nəzərə alınmır.
               
SELECT DISTINCT e.FIRST_NAME,
       e.LAST_NAME,
       e.JOB_ID,
       e.EMPLOYEE_ID

  FROM EMPLOYEES e 
  JOIN JOB_HISTORY jh 
    ON e.EMPLOYEE_ID = jh.EMPLOYEE_ID ;
   
--EXİSTS vasitəsilə alınan nəticəni   JOİN lə almaq üçün DİSTİNCT istifadə edə bilərik. 
-- Bunun üçün sətirlərin  təkrarlana bilməsi və həmin təkrar sətirlərin DİSTİNCT operatoru tərəfində silinməsi üçün
-- jh.JOB_İD sütununu sütunlar siyahısından çıxarmaq lazımdır.
   
--EXİSTS vasitəsilə alınan nəticəni SUBQUERY lə yazılması
   
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       E.JOB_ID,
       E.EMPLOYEE_ID
  FROM EMPLOYEES E
 WHERE E.EMPLOYEE_ID  IN (SELECT DISTINCT JH.EMPLOYEE_ID
                            FROM JOB_HISTORY JH
                           WHERE JH.EMPLOYEE_ID =E.EMPLOYEE_ID);

        

-- COMMON TABLE EXPRESSION (CTE)
-- non-recursive
-- recursive                          
                          
--SINGLE CTE USING WITH CLAUSE

--Əməkdaşların ad , soyad və çalışdıqları dep. üzrə əməkdaş sayı üzrə siyahı [USE JOIN]

SELECT E.FIRST_NAME,
       E.LAST_NAME,
       C.DEPT_COUNT,
       E.DEPARTMENT_ID 
  FROM EMPLOYEES E
  JOIN (SELECT E2.DEPARTMENT_ID,
               COUNT(E2.EMPLOYEE_ID) AS DEPT_COUNT
          FROM EMPLOYEES E2
      GROUP BY E2.DEPARTMENT_ID) C
    ON E.DEPARTMENT_ID = C.DEPARTMENT_ID; 
   
--Əməkdaşların ad , soyad və çalışdıqları dep. üzrə əməkdaş sayı üzrə siyahı [USE CTE]

WITH TDEPTS AS (
	 SELECT DEPARTMENT_ID,
	        COUNT(*) AS DEPT_COUNT
	   FROM EMPLOYEES 
   GROUP BY DEPARTMENT_ID
)
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       TDEPTS.DEPT_COUNT,
       e.DEPARTMENT_ID 
  FROM EMPLOYEES E
  JOIN TDEPTS 
    ON E.DEPARTMENT_ID = TDEPTS.DEPARTMENT_ID;
   
   
   SELECT * FROM tdepts;
                          
--MULTIPLE CTE USING WITH CLAUSE

                   
                          
--Difference Between CTE vs Subquery?
  
--CTEs can be recursive
 -- Əməkdaşlar arasında vəzifə üzrə asıllığın əyani nümayiş olunması
   
 WITH employee_chain(employee_id, first_name, last_name,chain_ ) AS (
  SELECT
    employee_id,
    first_name,
    last_name,
    first_name || ' ' || last_name AS chain_
  FROM employees
  WHERE manager_id IS NULL
  UNION ALL
  SELECT
    employees.employee_id,
    employees.first_name,
    employees.last_name,
    chain_ || '->' || employees.first_name || ' ' || employees.last_name
  FROM employee_chain
  JOIN employees
    ON employees.manager_id = employee_chain.employee_id
)
 
SELECT
  first_name,
  last_name,
  chain_
FROM employee_chain;

 --CTEs are reusable
 --Managerləri olan əməkdaşların ad və soyadı eləcə də managerlərin ad və soyadı əks etdirən hesabat. 
 --Bunun üçün CTE istifadə edərək ilk növbədə yalnız manageri olan əməkdaşları filter etmək lazım və daha sonra yuxarıda qeyd olunan hazırlanmalı
 
 WITH not_null_manager(EMPLOYEE_ID,MANAGER_ID,FIRST_NAME,LAST_NAME) AS(
   SELECT EMPLOYEE_ID,
          MANAGER_ID,
          FIRST_NAME,
          LAST_NAME 
     FROM EMPLOYEES e 
    WHERE E.MANAGER_ID IS NOT NULL 
 )
 SELECT E2.FIRST_NAME AS EMPLOYEE_FIRST_NAME,
        E2.LAST_NAME  AS EMPLOYEE_LAST_NAME,
        E2.EMPLOYEE_ID,
        E1.FIRST_NAME AS MANAGER_FIRST_NAME,
        E1.LAST_NAME  AS MANAGER_LAST_NAME,
        E1.EMPLOYEE_ID
        
   FROM NOT_NULL_MANAGER E1
   JOIN NOT_NULL_MANAGER E2 
     ON E1.EMPLOYEE_ID = E2.MANAGER_ID;
     
    
 --Eyni task subquery vasitəsilə həll yolu
 
 SELECT E2.FIRST_NAME  AS EMPLOYEE_FIRST_NAME,
        E2.LAST_NAME   AS EMPLOYEE_LAST_NAME,
        E2.EMPLOYEE_ID AS EMPLOYEE_ID,
        E1.FIRST_NAME  AS MANAGER_FIRST_NAME,
        E1.LAST_NAME   AS MANAGER_LAST_NAME,
        E1.EMPLOYEE_ID AS MANAGER_EMPLOYEE_ID
   FROM (SELECT * 
           FROM EMPLOYEES e
          WHERE E.MANAGER_ID IS NOT NULL ) E1
   JOIN (SELECT *
           FROM EMPLOYEES e
          WHERE E.MANAGER_ID IS NOT NULL ) E2
     ON E1.EMPLOYEE_ID =  E2.MANAGER_ID;     
 
--CTEs can be more readable    


