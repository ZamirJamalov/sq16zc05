
--DML haqda 
--melumatlarin daxil edilmesi haqda 
--sutun  adlarin yazilmadan daxil etmek haqda 
--sutun adlarinin ardcilliginin onemliliyi haqda  
--datatype conversion

--commit ve rollback ve sessiya  haqda 
--explicit commit and implicit commit


--basic select statement
--min. requirements
--pseudocolumns  rownum,rowid   -
--distinct or unique - 
--asterisk
--expressions
  --literals 
   --number
   --charecter or string 
   --Date 
   --Interval
 --operators and operator precedence 
 --functions

 --limit the rows   -
 --where clause
   --comparing expressions
   --comparing datatypes 
   --like  underscore and percent sign 

--boolean logic  -
 --and , or   prepare table 
--not 
--operator precedence 

--addtional where clause  -
--in
--between 
--is null, is not null

--sort the rows 
 --asc and desc 
--the column alias 
--reference by position -


--dual table
--concat , ||
--instr
--substr
--round
--trunc
--mod

--sysdate
--nvl
--case

--group functions 
--count sum min max avg 

--having clause






--DML haqda 
--melumatlarin daxil edilmesi haqda 
--sutun  adlarin yazilmadan daxil etmek haqda 
--sutun adlarinin ardcilliginin onemliliyi haqda  
--datatype conversion

--commit ve rollback ve sessiya  haqda 
--explicit commit and implicit commit
insert 
create table  xx 
rollback;

--basic select statement
--min. requirements
--pseudocolumns  rownum,rowid   -
--distinct or unique - 
--asterisk
--expressions
  --literals 
   --number
   --charecter or string 
   --Date 
   --Interval
 --operators and operator precedence 
 --functions

 --limit the rows   -
 --where clause
   --comparing expressions
   --comparing datatypes 
   --like  underscore and percent sign 

--boolean logic  -
 --and , or   prepare table 
--not 
--operator precedence 

--addtional where clause  -
--in
--between 
--is null, is not null

--sort the rows 
 --asc and desc 
--the column alias 
--reference by position -



select last_name,lower(last_name)
from employees where employee_id>=200;


select extract(year from hire_date),hire_date
   from employees;
   
  select first_name from employees where  
  extract(year from hire_date)=2003;
  
  select
     first_name||' '||last_name as "full name",salary,department_id
        from employees order by department_id,salary desc;
 
  select * from employees;
  
  select * from employees
    where first_name in ('steven','neena');
         
select * from  employees 
 where extract(year from hire_date)=2003 
   and (salary =3100 or salary=3500);
   

  select first_name,last_name,department_name,job_title
    from employees 
       join departments on employees.department_id=departments.department_id
       join jobs on employees.job_id=jobs.job_id;
       
       
      select first_name,last_name,
         nvl(department_name,'Diger')  as "department adi", 
             case department_name when 'Finance' then 'Maliyye' else department_name  end
    from departments 
       right join employees on employees.department_id=departments.department_id;

      
    select * from employees where department_id is null;
    
    select first_name,last_name,department_name from employees natural join  departments;
    
        select first_name,last_name,department_name from employees  join  departments using(manager_id);
    
    select first_name,last_name,department_name from employees join  departments 
     on employees.department_id =departments.department_id 
      and  employees.manager_id =departments.manager_id;
    
  select * from departments;
 select * from  employees 
 where extract(year from hire_date)=2003 
   or salary =3100 or salary=3500;  
   
   T F 
   T T T
   T F F
   F T F
   F F F
   
   T T T
   T F T
   F T T
   F F F 
   
