
--The PIVOT operator takes data in separate rows, aggregates it and converts it into columns.
--To see the PIVOT operator in action we need to create a test table.

drop table pivot_test;
/
CREATE TABLE pivot_test (
  id            NUMBER,
  customer_id   NUMBER,
  product_code  VARCHAR2(5),
  quantity      NUMBER
);

INSERT INTO pivot_test VALUES (1, 1, 'A', 10);
INSERT INTO pivot_test VALUES (2, 1, 'B', 20);
INSERT INTO pivot_test VALUES (3, 1, 'C', 30);
INSERT INTO pivot_test VALUES (4, 2, 'A', 40);
INSERT INTO pivot_test VALUES (5, 2, 'C', 50);
INSERT INTO pivot_test VALUES (6, 3, 'A', 60);
INSERT INTO pivot_test VALUES (7, 3, 'B', 70);
INSERT INTO pivot_test VALUES (8, 3, 'C', 80);
INSERT INTO pivot_test VALUES (9, 3, 'D', 90);
INSERT INTO pivot_test VALUES (10, 4, 'A', 100);
COMMIT;

/

select * from pivot_test;

--In its basic form the PIVOT operator is quite limited. We are forced to list the required values to PIVOT using the IN clause.

SELECT *
FROM   (SELECT product_code, quantity
        FROM   pivot_test)
PIVOT  (SUM(quantity) AS sum_quantity FOR (product_code) IN ('A' AS a, 'B' AS b, 'C' AS c));
/

--If we want to break it down by customer, we simply include the CUSTOMER_ID column in the initial select list.

SELECT *
FROM   (SELECT customer_id, product_code, quantity
        FROM   pivot_test)
PIVOT  (SUM(quantity) AS sum_quantity FOR (product_code) IN ('A' AS a, 'B' AS b, 'C' AS c))
ORDER BY customer_id;
/

--Prior to 11g we could accomplish a similar result using the DECODE function combined with aggregate functions.

SELECT SUM(DECODE(product_code, 'A', quantity, 0)) AS a_sum_quantity,
       SUM(DECODE(product_code, 'B', quantity, 0)) AS b_sum_quantity,
       SUM(DECODE(product_code, 'C', quantity, 0)) AS c_sum_quantity
FROM   pivot_test
ORDER BY customer_id;

/
--
SELECT customer_id,
       SUM(DECODE(product_code, 'A', quantity, 0)) AS a_sum_quantity,
       SUM(DECODE(product_code, 'B', quantity, 0)) AS b_sum_quantity,
       SUM(DECODE(product_code, 'C', quantity, 0)) AS c_sum_quantity
FROM   pivot_test
GROUP BY customer_id
ORDER BY customer_id;
/

--The UNPIVOT operator converts column-based data into separate rows. To see the UNPIVOT operator in action we need to create a test table.

drop table unpivot_test;
/

CREATE TABLE unpivot_test (
  id              NUMBER,
  customer_id     NUMBER,
  product_code_a  NUMBER,
  product_code_b  NUMBER,
  product_code_c  NUMBER,
  product_code_d  NUMBER
);

INSERT INTO unpivot_test VALUES (1, 101, 10, 20, 30, NULL);
INSERT INTO unpivot_test VALUES (2, 102, 40, NULL, 50, NULL);
INSERT INTO unpivot_test VALUES (3, 103, 60, 70, 80, 90);
INSERT INTO unpivot_test VALUES (4, 104, 100, NULL, NULL, NULL);
COMMIT;

/

select * from unpivot_test;
/
--The UNPIVOT operator converts this column-based data into individual rows.
SELECT *
FROM   unpivot_test
UNPIVOT (quantity FOR product_code IN (product_code_a AS 'A', product_code_b AS 'B', product_code_c AS 'C', product_code_d AS 'D'));
/


--By default the EXCLUDE NULLS clause is used. To override the default behaviour use the INCLUDE NULLS clause. 

SELECT *
FROM   unpivot_test
UNPIVOT INCLUDE NULLS (quantity FOR product_code IN (product_code_a AS 'A', product_code_b AS 'B', product_code_c AS 'C', product_code_d AS 'D'));
/

--Prior to 11g, we can get the same result using the DECODE function and a pivot table with the correct number of rows. 
--In the following example we use the CONNECT BY clause in a query from dual to generate the correct number of rows for the unpivot operation.

SELECT id,
       customer_id,
       DECODE(unpivot_row, 1, 'A',
                           2, 'B',
                           3, 'C',
                           4, 'D',
                           'N/A') AS product_code,
       DECODE(unpivot_row, 1, product_code_a,
                           2, product_code_b,
                           3, product_code_c,
                           4, product_code_d,
                           'N/A') AS quantity
FROM   unpivot_test,
       (SELECT level AS unpivot_row FROM dual CONNECT BY level <= 4)
ORDER BY 1,2,3;

/


--LISTAGGR

DROP TABLE emp;
/


CREATE TABLE emp (
  empno    NUMBER(4) CONSTRAINT pk_emp PRIMARY KEY,
  ename    VARCHAR2(10),
  job      VARCHAR2(9),
  mgr      NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  comm     NUMBER(7,2),
  deptno   NUMBER(2)
);

INSERT INTO emp VALUES (7369,'SMITH','CLERK',7902,to_date('17-12-1980','dd-mm-yyyy'),800,NULL,20);
INSERT INTO emp VALUES (7499,'ALLEN','SALESMAN',7698,to_date('20-2-1981','dd-mm-yyyy'),1600,300,30);
INSERT INTO emp VALUES (7521,'WARD','SALESMAN',7698,to_date('22-2-1981','dd-mm-yyyy'),1250,500,30);
INSERT INTO emp VALUES (7566,'JONES','MANAGER',7839,to_date('2-4-1981','dd-mm-yyyy'),2975,NULL,20);
INSERT INTO emp VALUES (7654,'MARTIN','SALESMAN',7698,to_date('28-9-1981','dd-mm-yyyy'),1250,1400,30);
INSERT INTO emp VALUES (7698,'BLAKE','MANAGER',7839,to_date('1-5-1981','dd-mm-yyyy'),2850,NULL,30);
INSERT INTO emp VALUES (7782,'CLARK','MANAGER',7839,to_date('9-6-1981','dd-mm-yyyy'),2450,NULL,10);
INSERT INTO emp VALUES (7788,'SCOTT','ANALYST',7566,to_date('13-7-87','dd-mm-yyyy'),3000,NULL,20);
INSERT INTO emp VALUES (7839,'KING','PRESIDENT',NULL,to_date('17-11-1981','dd-mm-yyyy'),5000,NULL,10);
INSERT INTO emp VALUES (7844,'TURNER','SALESMAN',7698,to_date('8-9-1981','dd-mm-yyyy'),1500,0,30);
INSERT INTO emp VALUES (7876,'ADAMS','CLERK',7788,to_date('13-8-87', 'dd-mm-yyyy'),1100,NULL,20);
INSERT INTO emp VALUES (7900,'JAMES','CLERK',7698,to_date('3-12-1981','dd-mm-yyyy'),950,NULL,30);
INSERT INTO emp VALUES (7902,'FORD','ANALYST',7566,to_date('3-12-1981','dd-mm-yyyy'),3000,NULL,20);
INSERT INTO emp VALUES (7934,'MILLER','CLERK',7782,to_date('23-1-1982','dd-mm-yyyy'),1300,NULL,10);
COMMIT;
/

--
select * from emp;
/

-- LISTAGG function, producing a comma-separated list of employees for each department.

SELECT deptno, LISTAGG(ename, ',') WITHIN GROUP (ORDER BY ename) AS employees
FROM   emp
GROUP BY deptno
ORDER BY deptno;
/

--listagg distinct Oracle 19

--Let's add some extra people called "MILLER" into department 10, to give us duplicates in the aggregated list.
INSERT INTO emp VALUES (9998,'MILLER','ANALYST',7782,to_date('23-1-1982','dd-mm-yyyy'),1600,NULL,10);
INSERT INTO emp VALUES (9999,'MILLER','MANADER',7782,to_date('23-1-1982','dd-mm-yyyy'),1500,NULL,10);
COMMIT;
/

--As expected, we now see multiple entries for the name "MILLER" in department 10.
SELECT deptno, LISTAGG(ename, ',') WITHIN GROUP (ORDER BY ename) AS employees
FROM   emp
GROUP BY deptno
ORDER BY deptno;
/

--If that's what we are expecting, great. If we want to remove duplicates, what do we do?

--We could solve this in a number of ways.
--In the following example we use the ROW_NUMBER analytic function to remove any duplicates, then use the conventional LISTAGG function to aggregate the data. PRE-19C

--
SELECT e2.deptno, LISTAGG(e2.ename, ',') WITHIN GROUP (ORDER BY e2.ename) AS employees
FROM   (SELECT e.*,
               ROW_NUMBER() OVER (PARTITION BY e.deptno, e.ename ORDER BY e.empno) AS myrank
        FROM   emp e) e2
WHERE  e2.myrank = 1
GROUP BY e2.deptno
ORDER BY e2.deptno;
/

--Alternatively we could use DISTINCT in an inline view to remove the duplicate rows, then use the conventional LISTAGG function call to aggregate the data.

SELECT e2.deptno, LISTAGG(e2.ename, ',') WITHIN GROUP (ORDER BY e2.ename) AS employees
FROM   (SELECT DISTINCT e.deptno, e.ename
        FROM   emp e) e2
GROUP BY e2.deptno
ORDER BY e2.deptno;
/

--Oracle 19c introduced a simpler solution. We can now include the DISTINCT keyword directly in the LISTAGG function call.
SELECT deptno, LISTAGG(DISTINCT ename, ',') WITHIN GROUP (ORDER BY ename) AS employees
FROM   emp
GROUP BY deptno
ORDER BY deptno;
/

--The default functionality is to include all results, which we can express explicitly using the ALL keyword.
SELECT deptno, LISTAGG(ALL ename, ',') WITHIN GROUP (ORDER BY ename) AS employees
FROM   emp
GROUP BY deptno
ORDER BY deptno;
/


--WITH CLAUSE

/*
  WITH <alias> AS (subquery-select-sql)
SELECT <column-list> FROM <alias>;

*/


--The advantage of using the WITH clause is that, once you have defined your subquery,
--you can subsequently reference it repeatedly in your main select statement


--Əməkdaşların ad , soyad və çalışdıqları dep. üzrə əməkdaş sayı üzrə siyahı [USE JOIN]
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       C.DEPT_COUNT,
       E.DEPARTMENT_ID 
  FROM EMPLOYEES E
  JOIN (SELECT E2.DEPARTMENT_ID,
               COUNT(E2.EMPLOYEE_ID) AS DEPT_COUNT
          FROM EMPLOYEES E2
      GROUP BY E2.DEPARTMENT_ID) C
    ON E.DEPARTMENT_ID = C.DEPARTMENT_ID; 
    
/

--Əməkdaşların ad , soyad və çalışdıqları dep. üzrə əməkdaş sayı üzrə siyahı [USE CTE]

WITH TDEPTS AS (
	 SELECT DEPARTMENT_ID,
	        COUNT(*) AS DEPT_COUNT
	   FROM EMPLOYEES 
   GROUP BY DEPARTMENT_ID
)
SELECT E.FIRST_NAME,
       E.LAST_NAME,
       TDEPTS.DEPT_COUNT,
       e.DEPARTMENT_ID 
  FROM EMPLOYEES E
  JOIN TDEPTS 
    ON E.DEPARTMENT_ID = TDEPTS.DEPARTMENT_ID;
/   
--Error   
   SELECT * FROM tdepts;
/



--Difference Between CTE vs Subquery?
  
--CTEs can be recursive
 -- Əməkdaşlar arasında vəzifə üzrə asıllığın əyani nümayiş olunması

WITH employee_chain(employee_id, first_name, last_name,chain_ ) AS (
  SELECT
    employee_id,
    first_name,
    last_name,
    first_name || ' ' || last_name AS chain_
  FROM employees
  WHERE manager_id IS NULL
  UNION ALL
  SELECT
    employees.employee_id,
    employees.first_name,
    employees.last_name,
    chain_ || '->' || employees.first_name || ' ' || employees.last_name
  FROM employee_chain
  JOIN employees
    ON employees.manager_id = employee_chain.employee_id
)
 
SELECT
  first_name,
  last_name,
  chain_
FROM employee_chain;

/



 --CTEs are reusable
 --Managerləri olan əməkdaşların ad və soyadı eləcə də managerlərin ad və soyadı əks etdirən hesabat. 
 --Bunun üçün CTE istifadə edərək ilk növbədə yalnız manageri olan əməkdaşları filter etmək lazım və daha sonra yuxarıda qeyd olunan hazırlanmalı

 WITH not_null_manager(EMPLOYEE_ID,MANAGER_ID,FIRST_NAME,LAST_NAME) AS(
   SELECT EMPLOYEE_ID,
          MANAGER_ID,
          FIRST_NAME,
          LAST_NAME 
     FROM EMPLOYEES e 
    WHERE E.MANAGER_ID IS NOT NULL 
 )
 SELECT E2.FIRST_NAME AS EMPLOYEE_FIRST_NAME,
        E2.LAST_NAME  AS EMPLOYEE_LAST_NAME,
        E2.EMPLOYEE_ID,
        E1.FIRST_NAME AS MANAGER_FIRST_NAME,
        E1.LAST_NAME  AS MANAGER_LAST_NAME,
        E1.EMPLOYEE_ID
        
   FROM NOT_NULL_MANAGER E1
   JOIN NOT_NULL_MANAGER E2 
     ON E1.EMPLOYEE_ID = E2.MANAGER_ID;
/


 --Eyni task subquery vasitəsilə həll yolu
 
 SELECT E2.FIRST_NAME  AS EMPLOYEE_FIRST_NAME,
        E2.LAST_NAME   AS EMPLOYEE_LAST_NAME,
        E2.EMPLOYEE_ID AS EMPLOYEE_ID,
        E1.FIRST_NAME  AS MANAGER_FIRST_NAME,
        E1.LAST_NAME   AS MANAGER_LAST_NAME,
        E1.EMPLOYEE_ID AS MANAGER_EMPLOYEE_ID
   FROM (SELECT * 
           FROM EMPLOYEES e
          WHERE E.MANAGER_ID IS NOT NULL ) E1
   JOIN (SELECT *
           FROM EMPLOYEES e
          WHERE E.MANAGER_ID IS NOT NULL ) E2
     ON E1.EMPLOYEE_ID =  E2.MANAGER_ID;
/




--RECURSIVE SUBQUERY 

-- oracle 10g
Select first_name ,employee_id ,manager_id from employees 
  connect by prior employee_id=manager_id 
  start with manager_id is null;
/  
--oracle 11g
with m (employee_id, first_name ,manager_id) as 
(select employee_id,first_name,manager_id 
  from employees 
   where manager_id is null 
  union all 
  select  e.employee_id as "manager_id",e.first_name,m.employee_id
        from employees e join m  on e.manager_id=m.employee_id
)
select * from m;
/


