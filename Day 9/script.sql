

--SET Operators
/*
Set operators combine the results of two component queries into a single result.
Queries set operators are called compound queries.

*/

--Union  - All distinct rows selected by either query.
select * from employees where salary between 5000 and 7000
union 
select * from employees where salary between 5500 and 6500;

--
104 -
123 -
124 -
166 -
167 -
173 -
179 -
202 -
203 -

--Union All -to retain the dublicate rows 
select * from employees where salary between 5000 and 7000 
union all 
select * from employees where salary between 5500 and 6500;

--Intersect -- it only returns the rows selected by all queries or data sets. If a record exists in one query and not in the other, it will be omitted from the INTERSECT results.
select* from employees where salary between 5000 and 7000
intersect 
select * from employees where salary between 5500 and 6500;


--
104
123
124
166
167
173
179
202
203

--Minus --The MINUS operator will retrieve all records from the first dataset and then remove from the results all records from the second dataset.
select* from employees where salary between 5000 and 7000
minus 
select * from employees where salary between 5500 and 6500;

--
113
155
161
165
178
--
select* from employees where salary between 5000 and 7000
minus 
select * from employees where salary between 5500 and 6500
minus
select * from employees where salary between 6500 and 6900;


--xeta verecek
select employee_id,first_name from employees where salary between 5000 and 7000
union 
select employee_id from employees where salary between 5500 and 6500;

--xeta verecek
select employee_id,first_name from employees where salary between 5000 and 7000
union 
select first_name,employee_id from employees where salary between 5500 and 6500;


--xeta vermeyecek
select  first_name,last_name from employees where salary between 5000 and 7000
union 
select last_name,first_name from employees where salary between 5500 and 6500;


--set operations (like UNION, UNION ALL, MINUS, INTERSECT) and JOINs have a key difference: set operations combine rows while joins combine columns
--
select * from employees where salary between 5000 and 7000
union all
select * from employees where salary between 5500 and 6500;

select * from  (select* from employees where salary between 5000 and 7000) a left join 
               (select * from employees where salary between 5500 and 6500) b
       on a.salary=b.salary;
     
       
 10000 50      
 
select max(e.salary) as maxsal , min(e.salary) as minsal, d.department_name from employees e , departments d 
                   where e.department_id=d.department_id group by  d.department_name having min(e.salary)<4000;

