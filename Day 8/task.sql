Table Employees. Get a list with information about all employees




Table Employees. Get a list of all employees with the name 'David'.



Table Employees. Get a list of all employees with job_id equal to 'IT_PROG'




Table Employees. Get a list of all employees from the 50th department (department_id) with a salary greater than 4000




Table Employees. Get a list of all employees from the 20th and 30th department (department_id)




Table Employees. Get a list of all employees whose last letter in their name equals 'a'.




Employees table. Get list of all employees from the 50th and from the 80th department (department_id) who have bonus (value in the commission_pct column is not empty)



Employees table. Get a list of all employees that have at least two 'n' letters in their name




Table Employees. Get a list of all employees whose name length is more than 4 letters




Table Employees. Get a list of all employees whose salary is between 8000 and 9000 (inclusive)




Employees. Get a list of all employees whose names contain the '%' symbol




Table Employees. Get list of all manager IDs




Table Employees. Get a list of employees with their positions in the format: Donald(sh_clerk)





Using Single-Row Functions to Customize Output

Table Employees. Get a list of all employees whose name length is more than 10 letters




Table Employees. Get a list of all employees with the letter 'b' in their names (not case sensitive)




Employees table. Get a list of all employees that have at least two letters 'a' in their name




Employees table. Get a list of all employees whose salary is a multiple of 1000




Employees table. Get the first 3-digit number of the employee''s phone number if its number is in the format ХХХ.ХХХ.ХХХ




Table Departments. Get the first word of the department name for the employees that have more than one word in their name



Employees table. Get the names of the employees without the first and the last letter in the name




Employees. Get a list of all employees, whose last letter in the name is 'm' and the length of the name is more than 5




Table Dual. Get next Friday''s date




Table Employees. Get a list of all employees who have been working for the company for more than 17 years



Translated with www.DeepL.com/Translator (free version)
