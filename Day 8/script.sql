/*
 Group by clause 

 Having Clause 

 self join 
 full join 

 
select * from employees group by first_name  --error

select * from employees order by first_name group by first_name; -error


select first_name from employees group by department_id; --error

select department_id from employees group by department_id; 

select count(*), department_id from employees group by department_id;


select min(salary),department_id from employees  group by department_id;

select min(salary),max(salary),department_id from employees group by department id;


select count(*),extract(year from hire_date)  as year from employees group by year;  --error


select count(*),extract(year from hire_date) as  il from  employees group by extract(year from  hire_date);



select count(*),extract(year from hire_date),department_id from employees 
  group by (extract(year from hire_date)),department_id 
    order by 2 desc,3 desc;

    


    Do not need to use the GROUP BY clause if only the GROUP FUNCTIONAL column is being used.
    GROUP FUNCTIONAL column ALIAS name can be used in the ORDER BY clause but not in the GROUP BY clause.
    GROUP FUNCTIONAL column always returns a single row result.




 the keyword rollup is used after    the keywords GROUP BY and is part of the group by clause
 the keyword rollup is followed by a grouping expression list enclosed in parentheses
rollup can repeated for each grouping in the group by clause you wish to roll up.

ROLLUP will find sub and grand total based on a single column.
CUBE will find sub and grand totals based on multiple columns.

SELECT count(*),department_id,job_id FROM employees GROUP BY rollup(department_id,job_id);
SELECT count(*),department_id,job_id FROM employees GROUP BY cube(department_id,job_id);

select count(*),extract(year from hire_date)
  from employees group by  rollup(extract(year from hire_date),department_id);
  
 burada department_id  select e elave olunmadigindan  iller uzre neticelerde hansinin dep-e aid oldugu hansinin ise dep-ler uzre cem oldugunu mueyyen etmek cetindir. bunun mueyyen etmek ucun grouping den istifade etmek lazimdir.

 select count(*),extract(year from hire_date),grouping(department_id)
  from employees group by  rollup(extract(year from hire_date),department_id);
  

  self join employees

  full join employees 


  table alias  in join  
   

   update  and delete statements 

   number presicion and scale

   employees cedveline yeni emekdas elave olunsun
   yeni department elave olunsun.

   having 

   tekrarlana  melumatlarin tapilmasi ucun istifade oluna biler 
   
*/
