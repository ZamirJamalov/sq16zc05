
--DML haqda 
--melumatlarin daxil edilmesi haqda 
--sutun  adlarin yazilmadan daxil etmek haqda 
--sutun adlarinin ardcilliginin onemliliyi haqda  
--datatype conversion

--commit ve rollback ve sessiya  haqda 
--explicit commit and implicit commit


--basic select statement
--min. requirements
--pseudocolumns  rownum,rowid
--distinct or unique
--asterisk
--expressions
  --literals 
   --number
   --charecter or string 
   --Date
   --Interval
 --operators and operator precedence 
 --functions

 --limit the rows 
 --where clause
   --comparing expressions
   --comparing datatypes 
   --like  underscore and percent sign 

--boolean logic 
 --and , or   prepare table 
--not 
--operator precedence 

--addtional where clause 
--in
--between 
--is null, is not null

--sort the rows 
 --asc and desc 
--the column alias 
--reference by position
