


--views
--Views- mentiqi cedveller adlandirilir. Onlar cedveller kimi ozlerinde melumat saxlamirlar.

--create view
--Bu sorgu tez-tez istifade olunursa, her defe bu sorgunu yazmaqdansa view halina getire ve daha sonra view istifade  oluna biler.
select e.first_name,e.last_name,e.salary,d.department_name from employees e join departments d on e.department_id=d.department_id;

create  or replace view emp_view as 
 select e.first_name,e.last_name,e.salary,d.department_name  from employees e join departments d on e.department_id=d.department_id;

--
drop view emp_view;
--view adlandirmasinda telebler cedveldeki teleblerle eynidir 

--tekrar emri icra etsek xeta lacagiq : ORA-00955

--view muraciet etmek
select * from emp_view;

--
drop view emp_depsales_view;

--melumatlari departmamentler uzre bolerek teqdim etmek
create view emp_depsales_view as 
  select e.employee_id,e.first_name,e.last_name,e.salary,e.email,e.hire_date,job_id,e.department_id from employees e where e.department_id=80;
  
  
--
select * from emp_depsales_view;

--View uzerinden melumatlari cedvelde deyismek mumkundur.
update emp_depsales_view e set e.first_name='John1' where  e.employee_id=145;

--
select * from emp_depsales_view;

--
rollback;


--baxmayaraq ki emp_depsales_view olan melumatlar 80 id li departamente aiddir ancaq diger id department melumatlarina da mudaxile oluna biler.

--  dep id tapaq
select * from departments;  --IT 60
--emp id tapaq
select * from employees where department_id=60; --Alexander 103

--melumat tapmayacaq ve netice deyismeyecek
update emp_depsales_view e set e.first_name='Alexander1' where  e.department_id=60;

select * from employees;

--
select  * from jobs;
--
insert into emp_depsales_view(employee_id,first_name,last_name,salary,email,hire_date,job_id,department_id) values(1000,'zamir','jamalov',50000,'jz@gmail.com',sysdate,'AD_PRES',60);

--yeni elave edilen data view da gorunmeyecek ancaq cedvelin ozunde goruncecek
select * from emp_depsales_view;

--
select * from employees;

--bele bir halin qarsisini almaq ucun view elave olaraq mehdudiyyet tetbiq olunmalidir
--
drop view emp_depsales_view;
--
create view emp_depsales_view as 
  select e.employee_id,e.first_name,e.last_name,e.salary,e.email,e.hire_date,job_id,e.department_id from employees e where e.department_id=80 with check option constraint emp_depsales_view_check1;
  
--yeniden melumati daxil etmeye calisaq. xeta alacagiq  ORA-01402
insert into emp_depsales_view(employee_id,first_name,last_name,salary,email,hire_date,job_id,department_id) values(1000,'zamir','jamalov',50000,'jz@gmail.com',sysdate,'AD_PRES',60);

--bundan evvelki deyisiklik normal icra olunacaq.
update emp_depsales_view e set e.first_name='John1' where  e.employee_id=145;

--
rollback;

--Eger melumatlarin deyisilmesini istemirikde o zaman view yaradan zaman bunu nezere almaq lazimdir.
create view emp_depsales_noupdateable_view as 
 select e.employee_id,e.first_name,e.last_name,e.salary from employees e where e.department_id=80 with read only;
 
--
select * from emp_depsales_noupdateable_view;

--xeta alacagiq ORA-42399
update emp_depsales_noupdateable_view  e set e.first_name='John1' where e.employee_id=145;


--view yaradan zaman eger cedveller her hansi biri movcud deyilse o zaman xeta verecek ORA-00942
create view emp_view1 as
 select id,name from emp_test;

--
drop view emp_view1;
--xeta vermemesi ucun
create force view emp_view1 as 
 select id,name from emp_test;

--ancaq view cagiranda xeta alacagiq o vaxta qeder ki emp_test cedveli yaradilmayib
select * from emp_view1;

--cedveli yaradib yeniden cehd edek;
create table emp_test (id number,name varchar2(50));

--artiq xeta  olmayacaq
select * from emp_view1;

--
drop table emp_test;

--view aliasla yaratmaq
create view emp_view2(fullname,maas,nomresi) as 
 select e.first_name||' '||e.last_name ,e.salary,e.employee_id from  employees e;
 
--
select * from emp_view2;

--
drop view emp_view2;

--
create view emp_view11 as 
 select * from employees where department_id>40;
 
create view emp_view22 as 
 select * from emp_view11 where employee_id between 100 and 130;
 
select * from emp_view22;

--
drop view emp_view11;
drop view emp_view22; 


--Temproray tables
/*
Global Temporary Tables : Available since Oracle 8i and subject of this article.
Private Temporary Tables : Available since Oracle 18c.
*/
--The data in a global temporary table is private, such that data inserted by a session can only be accessed by that session. 
--The ON COMMIT DELETE ROWS clause indicates the data should be deleted at the end of the transaction, or the end of the session.

CREATE GLOBAL TEMPORARY TABLE my_temp_table (
  id           NUMBER,
  description  VARCHAR2(20)
)
ON COMMIT DELETE ROWS;

-- Insert, but don't commit, then check contents of GTT.
INSERT INTO my_temp_table VALUES (1, 'ONE');

SELECT *  FROM my_temp_table;

-- Commit and check contents.
COMMIT;

--
SELECT COUNT(*) FROM my_temp_table;


--the ON COMMIT PRESERVE ROWS clause indicates that rows should persist beyond the end of the transaction. They will only be removed at the end of the session.
drop table my_temp_table;

CREATE GLOBAL TEMPORARY TABLE my_temp_table (
  id           NUMBER,
  description  VARCHAR2(20)
)
ON COMMIT PRESERVE ROWS;

-- Insert and commit, then check contents of GTT.
INSERT INTO my_temp_table VALUES (1, 'ONE');
COMMIT;

SELECT COUNT(*) FROM my_temp_table;


--
SELECT COUNT(*) FROM my_temp_table;


--Private temporary tables are memory-based
--The PRIVATE_TEMP_TABLE_PREFIX initialisation parameter, which defaults to "ORA$PTT_", defines the prefix that must be used in the name when creating the private temporary table

CREATE PRIVATE TEMPORARY TABLE my_temp_table (
  id           NUMBER,
  description  VARCHAR2(20)
);


--The ON COMMIT DROP DEFINITION clause, the default, indicates the table should be dropped at the end of the transaction, or the end of the session.
CREATE PRIVATE TEMPORARY TABLE ora$ptt_my_temp_table (
  id           NUMBER,
  description  VARCHAR2(20)
)
ON COMMIT DROP DEFINITION;


-- Insert, but don't commit, then check contents of PTT.
INSERT INTO ora$ptt_my_temp_table VALUES (1, 'ONE');

SELECT COUNT(*) FROM ora$ptt_my_temp_table;

-- Commit and check contents.
COMMIT;

SELECT COUNT(*) FROM ora$ptt_my_temp_table;
SELECT COUNT(*) FROM ora$ptt_my_temp_table;



--the ON COMMIT PRESERVE DEFINITION clause indicates the table and any data should persist beyond the end of the transaction. The table will be dropped at the end of the session.
CREATE PRIVATE TEMPORARY TABLE ora$ptt_my_temp_table (
  id           NUMBER,
  description  VARCHAR2(20)
)
ON COMMIT PRESERVE DEFINITION;


-- Insert, but don't commit, then check contents of PTT.
INSERT INTO ora$ptt_my_temp_table VALUES (1, 'ONE');

SELECT COUNT(*) FROM ora$ptt_my_temp_table;


--
COMMIT;

SELECT COUNT(*) FROM ora$ptt_my_temp_table;

-- Reconnect and check contents of GTT.


--create table as select
create table employee_backup as select* from employees;


--merge or upsert

merge into employee_backup e1  using 
(select * from employees ) e  on (e1.employee_id=e.employee_id)
when not  matched then 
  insert  (e1.employee_id,e1.first_name,e1.last_name,e1.salary,e1.email,e1.hire_date,e1.job_id,e1.department_id) 
    values(e.employee_id,e.first_name,e.last_name,e.salary,e.email,e.hire_date,e.job_id,e.department_id)
when   matched then 
  update  set first_name=e.first_name,last_name=e.last_name,salary=e.salary;
  
  
--
select * from employees ;
select * from employee_backup;
--
update employees set salary=24002 where employee_id=100;

--

--
merge into employee_backup e1  using 
(select * from employees ) e  on (e1.employee_id=e.employee_id)
when not  matched then 
  insert  (e1.employee_id,e1.first_name,e1.last_name,e1.salary,e1.email,e1.hire_date,e1.job_id,e1.department_id) 
    values(e.employee_id,e.first_name,e.last_name,e.salary,e.email,e.hire_date,e.job_id,e.department_id)
when   matched then 
  update  set first_name=e.first_name,last_name=e.last_name,salary=e.salary;
  
  
--
select * from employee_backup;


create table test1(id number, cnt number);
--
insert all 
   into test1 values(1,1)
   into test1 values(2,1)
select * from dual;   

--
select * from test1;

--
insert into  test1 values(1,1);
insert into  test1 values(2,1);
