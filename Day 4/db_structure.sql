

DROP TABLE phone;

DROP TABLE employer;

DROP TABLE work_place_job;

DROP TABLE job;

DROP TABLE work_place;

DROP TABLE address;

DROP TABLE course;


CREATE TABLE address (
    id   NUMBER PRIMARY KEY,
    name VARCHAR2(50)
);

CREATE TABLE course (
    id   NUMBER PRIMARY KEY,
    name VARCHAR2(50) UNIQUE
);

CREATE TABLE job (
    id   NUMBER PRIMARY KEY,
    name VARCHAR2(50) UNIQUE
);

CREATE TABLE work_place (
    id   NUMBER PRIMARY KEY,
    name VARCHAR2(50) UNIQUE
);

CREATE TABLE work_place_job (
    id            NUMBER PRIMARY KEY,
    job_id        NUMBER,
    work_place_id NUMBER,
    CONSTRAINT fk_work_place_job_job_id FOREIGN KEY ( job_id )
        REFERENCES job ( id ),
    CONSTRAINT fk_work_place_work_place_id FOREIGN KEY ( work_place_id )
        REFERENCES work_place ( id )
);

CREATE TABLE employer (
    emp_id        NUMBER PRIMARY KEY,
    first_name    VARCHAR2(50),
    last_name     VARCHAR2(50),
    work_place_id NUMBER,
    address_id    NUMBER,
    course_id     NUMBER,
    CONSTRAINT fk_employer_work_place_id FOREIGN KEY ( work_place_id )
        REFERENCES work_place ( id ),
    CONSTRAINT fk_employer_address_id FOREIGN KEY ( address_id )
        REFERENCES address ( id ),
    CONSTRAINT fk_employer_course_id FOREIGN KEY ( course_id )
        REFERENCES course ( id )
);

CREATE TABLE phone (
    emp_id NUMBER,
    phone  VARCHAR2(50),
    CONSTRAINT fk_phone_emp_id FOREIGN KEY ( emp_id )
        REFERENCES employer ( emp_id )
);
